export class Film {
    constructor(
        public title: string,
        public id: number,
        public overview: string,
        public budget: number,
        public release_date: string,
        public homepage: string
        
        ){}
}
