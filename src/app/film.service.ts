import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Film } from './film';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  private infos: any = [];
  private favoris: any = [];

  constructor(private http: HttpClient) { }

  public sendGetRequest(title: string, numPage: number) {
    return this.http.get(environment.ApiUrl + environment.API_TOKEN + '&language=fr&query=' + title + '&page=' + numPage);
  }

  public sendGetInformationRequest(idFilm: number) {
    return this.http.get(environment.apiFullMovieURL + idFilm + environment.API_TOKEN + '&language=fr')

  }
  public sendGetCreditsRequest(idFilm: number) {
    return this.http.get(environment.apiFullMovieURL + idFilm + environment.apiCreditsURL + environment.API_TOKEN + environment.apiLanguageFr)
  }


  getAllInfos() {
    return this.infos
  }

  addInfos(Film: Film) {
    this.infos = Film;
  }

  getAllFavoris() {
    this.favoris = JSON.parse(localStorage.getItem('FilmL') || '[]');

    return this.favoris

  }

  addFavoris(Film: Film) {
    let localFilm = localStorage.getItem('FilmL');
    console.log(localFilm);

    this.favoris = localStorage.getItem('FilmL') ? JSON.parse(localStorage.getItem('FilmL')) : [];


    console.log("favoris :", this.favoris);

    let bin = true;
    for (let i = 0; i < this.favoris.length; i++) {
      if (this.favoris[i].id == Film.id) {
        bin = false;
      }
    }
    if (bin == true) {
      this.favoris.push(Film);
      localStorage.setItem('FilmL', JSON.stringify(this.favoris));
    }
    console.log('add');
    this.favoris = this.getAllFavoris();
    location.reload()
  }

  removeFavoris(Film: Film) {
    console.log(Film.id);

    this.favoris = localStorage.getItem('FilmL') ? JSON.parse(localStorage.getItem('FilmL')) : [];
    for (let i = 0; i < this.favoris.length; i++) {
      if (Film.id == this.favoris[i].id) {
        this.favoris.splice(i, 1);
      }
    }

    localStorage.setItem('FilmL', JSON.stringify(this.favoris));
    this.favoris = this.getAllFavoris();
    location.reload()

  }
}
