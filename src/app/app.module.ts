import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ListeComponent } from './liste/liste.component';
import { FilmComponent } from './film/film.component';
import { HomeComponent } from './home/home.component';
import { FilmService } from './film.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ListeComponent,
    FilmComponent,
    HomeComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    
  ],
  providers: [
    FilmService,
  ],
  bootstrap: [AppComponent]
  
})
export class AppModule {
  
 }
